# Ruby Course 2023

NaUKMA Ruby on Rails programming course 2023

### Announcements

### Repository
* [Repository 2023 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2023)

### Course Materials
* [Telegram channel](http://t.me/naukma_ruby_2023)
* [Repository 2022 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2022)
* [Repository 2021 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2021)
* [Repository 2020 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2020)
* [Repository 2019 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2019)
* [Repository 2017 (BitBucket)](https://bitbucket.org/burius/ruby_course_2017)
* [Repository 2016 (BitBucket)](https://bitbucket.org/burius/ror_course)

### The Project
* TBD

### Useful Links
#### Ruby/Rails
* [Official Ruby documentation (core API)](https://ruby-doc.org/3.2.0/)
* [Ruby style guide](https://rubystyle.guide/) by bbatsov
* [Official Rails Guides](https://guides.rubyonrails.org/)
* [Rails API documentation](https://api.rubyonrails.org/)
