require 'roda'

class App < Roda
  plugin :all_verbs
  plugin :render

  route do |r|
    r.get 'hello' do
      'Hello from Roda!'
    end
    r.on 'api' do
      @a = 2 + 2
      r.get 'home' do
        view('home')
      end
      r.put 'home' do
        "putting #{@a}"
      end
    end
  end
end
